from troposphere import Template, Parameter, Ref, Output
from troposphere.ec2 import SecurityGroup

class WpInstanceSg():
    def __init__(self, sceptre_user_data):
        self.template = Template()
        self.resource_name = sceptre_user_data['resource_name']
        self.ingress_rule_80 = {
            'IpProtocol' : 'tcp',
            'FromPort' : 80,
            'ToPort' : 80,
            'CidrIp' : '0.0.0.0/0'
            }

        self.ingress_rule_443 = {
            'IpProtocol' : 'tcp',
            'FromPort' : 443,
            'ToPort' : 443,
            'CidrIp' : '0.0.0.0/0'
            }
        self.vpc_id = self.template.add_parameter(Parameter(
            "VpcId",
            Type="String"
        ))
        self.wp_instance_sg = self.template.add_resource(SecurityGroup(
            self.resource_name,
            GroupDescription="Allows Alb and EC2 to receive http or https traffic",
            SecurityGroupIngress=[self.ingress_rule_80, self.ingress_rule_443],
            VpcId=Ref(self.vpc_id)
        ))
        self.template.add_output(Output(
            "wpInstanceSg",
            Value=Ref(self.wp_instance_sg)
        ))

def sceptre_handler(sceptre_user_data):
    return WpInstanceSg(sceptre_user_data).template.to_yaml()
