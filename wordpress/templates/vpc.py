from troposphere import Template, Parameter, Ref, Output, Tags
from troposphere.ec2 import VPC, VPCGatewayAttachment, InternetGateway, RouteTable, Route
from base import BaseResource

class Vpc(BaseResource):
    def __init__(self, sceptre_user_data):
        BaseResource.__init__(self, sceptre_user_data)

        self.resource_name = self.sceptre_user_data['resource_name']

        self.add_params()
        self.add_resources()
        self.add_outputs()

    def add_params(self):
        self.cidr_block = self.template.add_parameter(Parameter(
            "cidrBlock",
            Type="String"
        ))
    def add_resources(self):
        self.vpc = self.template.add_resource(VPC(
            self.resource_name,
            CidrBlock=Ref(self.cidr_block),
            Tags=[*self.tags]
        ))

        if 'active_ig' in self.sceptre_user_data and self.sceptre_user_data['active_ig']:
            self.vpc_ig = self.template.add_resource(InternetGateway("wordpressIg"))
            self.template.add_resource(VPCGatewayAttachment(
                "WpVpc2IgAttachment",
                InternetGatewayId=Ref(self.vpc_ig),
                VpcId=Ref(self.vpc)
            ))
            #creating a common routing public table
            self.wp_pub_subnet_rt = self.template.add_resource(RouteTable(
                "WpPublicSubnetRouteTable",
                VpcId=Ref(self.vpc),
                Tags=[*self.tags]
            ))
            #create public route
            self.template.add_resource(Route(
                "WpPublicSubnetRoute",
                RouteTableId=Ref(self.wp_pub_subnet_rt),
                GatewayId=Ref(self.vpc_ig),
                DestinationCidrBlock=self.sceptre_user_data['DestinationCidrBlock']
            ))

    def add_outputs(self):
        if 'active_ig' in self.sceptre_user_data and self.sceptre_user_data['active_ig']:
            self.template.add_output(Output(
                "Ig",
                Value=Ref(self.vpc_ig)
            ))
            self.template.add_output(Output(
                "PubSubnetRT",
                Value=Ref(self.wp_pub_subnet_rt)
            ))

        self.template.add_output(Output(
            "VpcId",
            Value=Ref(self.vpc)
        ))

def sceptre_handler(sceptre_user_data):
    return Vpc(sceptre_user_data).template.to_yaml()
