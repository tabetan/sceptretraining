from troposphere import Template, Parameter, Ref

class BaseResource():
    def __init__(self, sceptre_user_data):
        self.sceptre_user_data = sceptre_user_data
        self.template = Template()
        self.add_default_parameters()

    def add_default_parameters(self):
        self.tags = list()
        self.tag_department = self.template.add_parameter(Parameter(
            title="TagDepartment",
            Type="String"
        ))
        self.tags.append(self.produceTagsList("TagDepartment", Ref(self.tag_department)))
        self.tag_application = self.template.add_parameter(Parameter(
            title="TagApplication",
            Type="String"
        ))
        self.tags.append(self.produceTagsList("TagApplication", Ref(self.tag_application)))

    def produceTagsList(self, label, param_value):
        return {"Key": label, "Value": param_value}
