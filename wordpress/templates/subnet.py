from troposphere import Template, Parameter, Ref, GetAZs, Select, Output
from troposphere.ec2 import Subnet, SubnetRouteTableAssociation
from base import BaseResource

class WpSubnet(BaseResource):
    def __init__(self, sceptre_user_data):
        BaseResource.__init__(self, sceptre_user_data)
        self.resource_name = self.sceptre_user_data['resource_name']
        self.add_params()
        self.add_resources()
        self.add_outputs()

    def add_params(self):
        self.cidr_block = self.template.add_parameter(Parameter("CidrBlock", Type="String"))
        self.vpc_id = self.template.add_parameter(Parameter("VpcId", Type="String"))
        self.pub_subnet_rt = self.template.add_parameter(Parameter("PubSubnetRT", Type="String"))

    def add_resources(self):
        self.wp_subnet = self.template.add_resource(Subnet(
            self.resource_name,
            CidrBlock=Ref(self.cidr_block),
            VpcId=Ref(self.vpc_id),
            AvailabilityZone=Select(int(self.sceptre_user_data["AZindex"]), GetAZs("")),
            Tags=[*self.tags]
        ))
        if self.sceptre_user_data['subnetType'] == 'public':
            self.template.add_resource(SubnetRouteTableAssociation(
                "WpSubnet2RouteTableAssociation",
                RouteTableId=Ref(self.pub_subnet_rt),
                SubnetId=Ref(self.wp_subnet)
            ))

    def add_outputs(self):
        self.template.add_output(Output(
            "subId",
            Value=Ref(self.wp_subnet)
        ))

def sceptre_handler(sceptre_user_data):
    return WpSubnet(sceptre_user_data).template.to_yaml()
