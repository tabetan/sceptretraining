from troposphere import Parameter, Ref, Output, Join, Base64
from troposphere.ec2 import Instance
from base import BaseResource


class WpEC2(BaseResource):
    def __init__(self, sceptre_user_data):
        BaseResource.__init__(self, sceptre_user_data)

        self.resource_name = self.sceptre_user_data['resource_name']

        self.add_params()
        self.add_resources()
        self.add_outputs()

    def add_params(self):
        self.private_subnet_id = self.template.add_parameter(Parameter(
            "priSubId",
            Type="String"
        ))
        self.wp_instance_sg = self.template.add_parameter(Parameter(
            "wpInstanceSg",
            Type="String"
        ))
        self.db_name = self.template.add_parameter(Parameter(
            "DBName",
            Type="String"
        ))
        self.db_url = self.template.add_parameter(Parameter(
            "DBUrl",
            Type="String"
        ))
        self.master_username = self.template.add_parameter(Parameter(
            "MasterUsername",
            Type="String"
        ))
        self.master_password = self.template.add_parameter(Parameter(
            "MasterPassword",
            Type="String"
        ))
    def add_resources(self):
        self.ec2_instance = self.template.add_resource(Instance(
            self.resource_name,
            ImageId=self.sceptre_user_data['amiId'],
            SubnetId=Ref(self.private_subnet_id),
            SecurityGroupIds=[Ref(self.wp_instance_sg)],
            Tags=[*self.tags],
            UserData=Base64(Join('', [
                "#!/bin/bash\n",
                "cd /var/www/html\n",
                "cp wp-config-sample.php wp-config.php\n",
                "sed -i \"s/database_name_here/", Ref(self.db_name), "/g\" wp-config.php\n",
                "sed -i \"s/username_here/", Ref(self.master_username), "/g\" wp-config.php\n",
                "sed -i \"s/password_here/", Ref(self.master_password), "/g\" wp-config.php\n",
                "sed -i \"s/localhost/", Ref(self.db_url), "/g\" wp-config.php\n"
            ]))

            # cd /var/www/html
            # cp wp-config-sample.php wp-config.php
            # sed -i "s/database_name_here/$wp_db_name/g" wp-config.php
            # sed -i "s/username_here/$wp_db_username/g" wp-config.php
            # sed -i "s/password_here/$wp_db_password/g" wp-config.php
            # sed -i "s/localhost/$wp_db_url/g" wp-config.php

        ))
    def add_outputs(self):
        self.template.add_output(Output(
            "WpEc2Instance",
            Value=Ref(self.ec2_instance)
        ))

def sceptre_handler(sceptre_user_data):
    return WpEC2(sceptre_user_data).template.to_yaml()
