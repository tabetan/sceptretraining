"""Application load balancer, internet facing with two public subnets"""

from troposphere import Template, Ref, Parameter, Output
from troposphere.elasticloadbalancingv2 import LoadBalancer, TargetGroup, TargetDescription, Listener, Action
from base import BaseResource

class WpAlb(BaseResource):
    def __init__(self, sceptre_user_data):
        BaseResource.__init__(self, sceptre_user_data)

        self.add_params()
        self.add_resources()
        self.add_outputs()

    def add_params(self):
        self.vpc_id = self.template.add_parameter(Parameter("VpcId", Type="String"))
        self.wp_pub_sub1 = self.template.add_parameter(Parameter("WpPubSub1", Type="String"))
        self.wp_pub_sub2 = self.template.add_parameter(Parameter("WpPubSub2", Type="String"))
        self.wp_sg = self.template.add_parameter(Parameter("wpInstanceSg", Type="String"))
        self.wp_instance_1 = self.template.add_parameter(Parameter(
            "WpEc2Instance1",
            Type="String"
        ))
        self.wp_instance_2 = self.template.add_parameter(Parameter(
            "WpEc2Instance2",
            Type="String"
        ))

    def add_resources(self):
        self.resource_name = self.sceptre_user_data['resource_name']
        self.wp_alb = self.template.add_resource(LoadBalancer(
            self.resource_name,
            Name=self.resource_name,
            Scheme="internet-facing",
            Type="application",
            Subnets=[Ref(self.wp_pub_sub1), Ref(self.wp_pub_sub2)],
            SecurityGroups=[Ref(self.wp_sg)],
            Tags=[*self.tags]
        ))
        self.wp_target_grp = self.template.add_resource(TargetGroup(
            "WpTargetGroup",
            Port=80,
            Protocol='HTTP',
            VpcId=Ref(self.vpc_id),
            TargetType='instance',
            Targets=[TargetDescription(
                Id=Ref(self.wp_instance_1),
                Port=80
            ), TargetDescription(
                Id=Ref(self.wp_instance_2),
                Port=80
            )],
            Tags=[*self.tags]
        ))
        self.template.add_resource(Listener(
            "WpListener",
            LoadBalancerArn=Ref(self.wp_alb),
            Port=80,
            Protocol='HTTP',
            DefaultActions=[
                Action(
                    "ListenerForwardAction",
                    Type="forward",
                    TargetGroupArn=Ref(self.wp_target_grp)
                )
            ]
        ))
    def add_outputs(self):
        self.template.add_output(Output(
            "AlbId",
            Value=Ref(self.wp_alb)
        ))

def sceptre_handler(sceptre_user_data):
    return WpAlb(sceptre_user_data).template.to_yaml()
