from base import BaseResource
from troposphere import Parameter, Output, Template, Ref, GetAtt
from troposphere.rds import DBInstance, DBSubnetGroup, DBSecurityGroup

class WpRds(BaseResource):
    def __init__(self, sceptre_user_data):
        BaseResource.__init__(self, sceptre_user_data)
        self.resource_name = self.sceptre_user_data['resource_name']

        self.add_params()
        self.add_resources()
        self.add_outputs()

    def add_params(self):
        self.private_subnet_1 = self.template.add_parameter(Parameter(
            "PrivateSubnet1",
            Type="String"
        ))
        self.private_subnet_2 = self.template.add_parameter(Parameter(
            "PrivateSubnet2",
            Type="String"
        ))
        self.db_instance_identifier = self.template.add_parameter(Parameter(
            "DBInstanceIdentifier",
            Type="String"
        ))
        self.master_username = self.template.add_parameter(Parameter(
            "MasterUsername",
            Type="String"
        ))
        self.master_password = self.template.add_parameter(Parameter(
            "MasterPassword",
            Type="String"
        ))
        self.db_name = self.template.add_parameter(Parameter(
            "DBName",
            Type="String"
        ))
        self.vpc_id = self.template.add_parameter(Parameter(
            "VpcId",
            Type="String"
        ))
        self.ec2_sg = self.template.add_parameter(Parameter(
            "Ec2Sg",
            Type="String"
        ))

    def add_resources(self):
        self.ingress_rule_db = {
            'EC2SecurityGroupId' : Ref(self.ec2_sg)
        }
        self.db_sg = self.template.add_resource(DBSecurityGroup(
            self.resource_name+"SG",
            EC2VpcId=Ref(self.vpc_id),
            GroupDescription="Authorizing wordpress EC2 instances",
            DBSecurityGroupIngress=[self.ingress_rule_db],
            Tags=[*self.tags]
        ))
        db_subnet_grp_name = (self.resource_name+"subgrpname").lower()
        self.template.add_resource(DBSubnetGroup(
            title=self.resource_name+"SubnetGroup",
            DBSubnetGroupDescription="Private subnet hosting RDS instances",
            DBSubnetGroupName=db_subnet_grp_name,
            SubnetIds=[Ref(self.private_subnet_1), Ref(self.private_subnet_2)]
        ))
        self.wp_db = self.template.add_resource(DBInstance(
            title=self.resource_name,
            DBInstanceClass="db.t2.micro", #pass as parameter use mapping
            Engine="MySQL", #pass as parameter
            MasterUsername=Ref(self.master_username),
            MasterUserPassword=Ref(self.master_password),
            DBInstanceIdentifier=Ref(self.db_instance_identifier),
            AllocatedStorage=12,
            DBSubnetGroupName=db_subnet_grp_name,
            DBName=Ref(self.db_name),
            DBSecurityGroups=[Ref(self.db_sg)],
            Tags=[*self.tags]
        ))

    def add_outputs(self):
        self.template.add_output(Output(
            "MasterUsername",
            Value=Ref(self.master_username)
        ))
        self.template.add_output(Output(
            "MasterPassword",
            Value=Ref(self.master_password)
        ))
        self.template.add_output(Output(
            "DBName",
            Value=Ref(self.db_name)
        ))
        self.template.add_output(Output(
            "DBUrl",
            Value=GetAtt(self.wp_db, attrName="Endpoint.Address")
        ))
def sceptre_handler(sceptre_user_data):
    return WpRds(sceptre_user_data).template.to_yaml()
