#!/bin/bash
yum -y update
yum -y upgrade
yum -y install httpd
yum -y install jq
systemctl enable httpd.service
yum -y install php
#Installing all php modules
search_result=`yum search php-`
for tbl in $search_result
do
   if [[ $tbl =~ ^php-.+$ ]]
   then
        yum -y install $tbl
   fi
done
systemctl restart httpd.service
cd ~
wget http://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
sudo rsync -avP ~/wordpress/ /var/www/html/
mkdir /var/www/html/wp-content/uploads
chown -R apache:apache /var/www/html/*

exit 0
