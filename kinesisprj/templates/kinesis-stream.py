from troposphere import Template, Parameter, Ref
from troposphere.kinesis import Stream, StreamEncryption


class KinesisStream(object):
    def __init__(self, sceptre_user_data):
        self.template = Template()
        self.sceptre_user_data = sceptre_user_data
        self.add_stream_parameters()
        self.add_enc_stream_parameters()
        if "streamEncryption" in self.sceptre_user_data and self.sceptre_user_data.get("streamEncryption") == True:
            self.add_kinesis_enc()
            self.add_kinesis_stream(encActive=True)
        else:
            self.add_kinesis_stream(encActive=False)

    def add_stream_parameters(self):
        self.param_streamName = self.template.add_parameter(Parameter(
            "streamName",
            Type="String"
        ))
        self.param_retentionHours = self.template.add_parameter(Parameter(
            "retentionHours",
            Type="Number"
        ))
        self.param_shardCount = self.template.add_parameter(Parameter(
            "shardCount",
            Type="Number"
        ))

    def add_kinesis_stream(self, encActive):
        kstream = self.template.add_resource(Stream(
            "KinesisTwitter",
            Name = Ref(self.param_streamName),
            RetentionPeriodHours = Ref(self.param_retentionHours),
            ShardCount = Ref(self.param_shardCount)
        ))
        if encActive :
            kstream.StreamEncryption = self.kinesis_enc

    def add_enc_stream_parameters(self):
        self.param_encType = "KMS"
        self.param_myKey = self.template.add_parameter(Parameter(
            "myKey",
            Type="String"
        ))

    def add_kinesis_enc(self):
        self.kinesis_enc = StreamEncryption(
        EncryptionType = self.param_encType,
        KeyId = Ref(self.param_myKey)
        )

def sceptre_handler(sceptre_user_data):
    kstream = KinesisStream(sceptre_user_data)
    return kstream.template.to_yaml()
